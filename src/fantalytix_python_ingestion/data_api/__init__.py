import os
from flask import Flask

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app_settings = os.getenv(
        'APP_SETTINGS',
        'fantalytix_python_ingestion.data_api.config.DevelopmentConfig'
    )
    app.config.from_object(app_settings)

    from . import db
    db.init_app(app)

    from . import api
    app.register_blueprint(api.bp)

    return app
