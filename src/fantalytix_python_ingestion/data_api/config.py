import os

base_dir = os.path.abspath(os.path.dirname(__file__))
local_db_base = 'postgresql+psycopg2://postgres:postgrespassword@localhost:5432/'
db_name = 'fantalytix'


class BaseConfig:
    """
    Base application configuration
    """
    DEBUG = False
    SECRET_KEY = os.getenv('SECRET_KEY', 'my_strong_key')


class DevelopmentConfig(BaseConfig):
    """
    Development application configuration
    """
    DEBUG = True
    ENVIRONMENT = 'DEVELOPMENT'
    SQLALCHEMY_DATABASE_URI = os.getenv(
        'DATABASE_URL', 
        local_db_base + db_name
    )


class TestingConfig(BaseConfig):
    """
    Testing application configuration
    """
    DEBUG = True
    TESTING = True
    ENVIRONMENT = 'TESTING'
    SQLALCHEMY_DATABASE_URI = os.getenv(
        'DATABASE_URL_TEST', 
        local_db_base + db_name + "-test"
    )


class ProductionConfig(BaseConfig):
    """
    Production application configuration
    """
    DEBUG = True
    ENVIRONMENT = 'PRODUCTION'
    SQLALCHEMY_DATABASE_URI = os.getenv(
        'DATABASE_URL', 
        local_db_base + db_name
    )
