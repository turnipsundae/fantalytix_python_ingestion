from datetime import date

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for,
    jsonify
)

from fantalytix_sqlalchemy.orm.common import (
    League, Season
)

from .db import get_db
from .utils import (
    crossdomain, get_or_create, commit_or_400, validate
)
from .schema import LeagueSchema, SeasonSchema

league_schema = LeagueSchema(strict=True)
season_schema = SeasonSchema(strict=True)
bp = Blueprint('api', __name__, url_prefix='/api')

@bp.route('/leagues', methods=['GET', 'POST', 'PUT', 'DELETE'])
@crossdomain(origin='http://localhost:3000')
def leagues():
    data = request.get_json()
    db = get_db()

    if request.method == 'GET':
        results = league_schema.dump(db.query(League).all(), many=True)
        return jsonify({'data': results.data})

    elif request.method == 'POST':
        instances = [get_or_create(db, League, **row) for row in data['data']]
        new_data = [instance for instance, created in instances if created is True] 
        commit_or_400(db, msg='Could not create objects')
        results = league_schema.dump(new_data, many=True)
        return jsonify({'data': results.data})

    elif request.method == 'DELETE':
        for row in data['data']:
            db.query(League).filter_by(**row).delete()
        commit_or_400(db, msg='Could not delete objects')
        return 'Successfully deleted items', 204


@bp.route('/leagues/abbreviation/<string:abbreviation>', methods=['GET'])
@crossdomain(origin='http://localhost:3000')
def leagues_abbreviation(abbreviation):
    try:
        results = league_schema.dump(
            get_db().query(League).filter_by(abbreviation=abbreviation).first()
        )
    except AttributeError:
        return jsonify({})

    return jsonify({'data': results.data})

@bp.route('/seasons', methods=['GET'])
@crossdomain(origin='http://localhost:3000')
def seasons():
    results = season_schema.dump(get_db().query(Season).all(), many=True)

    return jsonify(results.data)

@bp.route('/seasons/league/<string:abbreviation>/endyear/<int:end_year>', methods=['GET'])
@crossdomain(origin='http://localhost:3000')
def seasons_league_endyear(abbreviation, end_year):
    try:
        results = season_schema.dump(
            get_db().query(Season).filter(
                League.abbreviation == abbreviation,
                Season.end_year == date(end_year, 1, 1)
            ).first()
        )
    except AttributeError:
        return jsonify({})

    return jsonify(results.data)
