from .test_api import TestAPI

class TestLeaguesAPI(TestAPI):

    def setUp(self):
        super().setUp()
        super().setUpLeaguesTable()

    def tearDown(self):
        super().tearDownLeaguesTable()
        super().tearDown()

    def test_leagues_get(self):
        resp = self.client.get(self.LEAGUE_API_URL)
        self.assertEqual(
            resp.get_json(), 
            {
                'data': [
                    self.LEAGUE_NBA,
                    self.LEAGUE_BAA,
                ],
            }
        )

    def test_leagues_post(self):
        resp = self.client.post(self.LEAGUE_API_URL, json={'data': [self.LEAGUE_ABA]})
        self.assertEqual(resp.get_json(), {'data': [self.LEAGUE_ABA]})

    def test_leagues_delete(self):
        resp = self.client.delete(self.LEAGUE_API_URL, json={'data': [self.LEAGUE_NBA]})
        self.assertIs(resp.get_json(), None)
        self.assertEqual(resp.status_code, 204)
        resp = self.client.get(self.LEAGUE_API_URL)
        self.assertEqual(resp.get_json(), {'data': [self.LEAGUE_BAA]})

    def test_leagues_abbreviation_get(self):
        resp = self.client.get(self.LEAGUE_NBA_API_URL)
        self.assertEqual(resp.get_json(), {'data': self.LEAGUE_NBA})

    def test_leagues_abbreviation_post(self):
        return
        resp = self.client.post(
            self.LEAGUE_NBA_API_URL, 
            json={'data': self.data}
        )
        self.assertEqual(resp.status_code, 405)

    def test_leagues_abbreviation_delete(self):
        return
        resp = self.client.delete(self.LEAGUE_NBA_API_URL)
        self.assertEqual(resp.get_json(), {})
        self.assertEqual(resp.status_code, 204)

if __name__ == '__main__':
    unittest.main()
